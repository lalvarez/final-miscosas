from django.contrib import admin

# Register your models here.
from .models import Alimentador, Item, Vote, Image, Style, Comment

admin.site.register(Alimentador)
admin.site.register(Item)
admin.site.register(Vote)
admin.site.register(Image)
admin.site.register(Style)
admin.site.register(Comment)
