from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
class Alimentador(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    title = models.CharField(max_length=64, null=True)
    link = models.CharField(max_length=64, null=True)
    num_items = models.IntegerField(default=0)
    puntuacion = models.IntegerField(default=0)
    selected = models.BooleanField(default = True)
    tipo = models.CharField(max_length = 64, null=True)
    def __str__(self):
        return self.title

class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=64, null=True)
    link = models.CharField(max_length=64, null=True)
    id = models.CharField(max_length=64, primary_key=True)
    description = models.CharField(max_length=256, null=True)
    positivos = models.IntegerField(default=0)
    negativos = models.IntegerField(default=0)
    puntuacion = models.IntegerField(default=0)


    def __str__(self):
        return self.title

class Vote(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    item = models.ForeignKey(Item,on_delete=models.CASCADE)
    voto = models.IntegerField(default=0)

    def __str__(self):
        return 'El user que ha votado ' + self.user.username + ' Con items: ' + self.item.title
class Image(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, null = True)
    photo = models.ImageField(upload_to = 'media', default="nopic.jpg")

class Comment(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    item = models.ForeignKey(Item,on_delete=models.CASCADE)
    title = models.CharField(max_length = 50, null = True)
    comment = models.CharField(max_length=250, null = True)
    date= models.DateTimeField(default = timezone.now)
    photourl = models.URLField(null = True)


    def __str__(self):
        return self.comment

class Style(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    oscuro =  models.BooleanField(default = False)
    letra = models.CharField(max_length = 2, default = 'M')
