from django import forms
from django.contrib.auth.models import User
from .models import Alimentador, Image

class UploadImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('photo',)
