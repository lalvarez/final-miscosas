from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string

from .models import Alimentador, Item
class RedditHandler(ContentHandler):
    """Class to handle events fired by the SAX parser
    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        self.description= ""
        self.item = []
        self.subreddit = ""
        self.subredditLink = ""
        self.subredditName = ""
        self.subredditLabel = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
            try:
                subreddit = Alimentador.objects.get(title=self.subredditName)
                self.subreddit = subreddit
                self.subreddit.tipo = "rd"
            except Alimentador.DoesNotExist:
                subreddit = Alimentador(id=self.subredditLabel,title=self.subredditName, link=self.subredditLink,
                            num_items=len(self.item), puntuacion=0, selected = True, tipo = "rd")
                self.subreddit = subreddit
            subreddit.save()
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'id':
                self.inContent = True
            elif name == 'content':
                self.inContent = True
        else:
            if name == 'category':
                self.subredditLabel = attrs.get('term')
            elif name == 'link' and attrs.get('rel') == 'alternate':
                self.subredditLink = attrs.get('href')
            elif name == 'title':
                self.inContent = True

    def endElement (self, name):
        if name == 'entry':
            self.inEntry = False
            try:
                item = Item.objects.get(id = self.id)
                self.item.append(item)
            except:
                item = Item(link=self.link, title=self.title, id=self.id, description=self.description,
                                positivos=0, negativos=0, alimentador=self.subreddit)
                item.save()
                self.item.append(item)
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
            elif name == 'content':
                self.description = self.content
            elif name == 'id':
                self.id = self.content

        else:
            if name == 'title':
                self.subredditName = self.content

        self.content = ""
        self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class Reddit:
    """Class to get notices in a subreddit.
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = RedditHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)
