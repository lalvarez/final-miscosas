from django.urls import path, include
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.index),
    path('info', views.info),
    path('logout', views.logout_view),
    path('register', views.register),
    path('youtube', views.youtubeal),
    path('youtube/<str:id>', views.youtubechannel),
    path('reddit', views.reddital),
    path('usuarios', views.usuarios),
    path('youtube/<str:canal_id>/<str:item_id>', views.youtubevid),
    path('reddit/<str:id>', views.subreddit),
    path('reddit/<str:subreddit_id>/<str:item_id>', views.redditnot),
    path('letras', views.letras),
    path('oscuro', views.oscuro),
    path('vote/<str:item_id>', views.vote),
    path('comment/<str:item_id>', views.comment),
    path('alimentadores', views.alimentadores),
    path('delete', views.delete),
    path('loggedIn', views.loggedIn),
    path('<str:username>', views.user),
    path('i18n/', include('django.conf.urls.i18n')),

]
