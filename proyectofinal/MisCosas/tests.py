from django.test import TestCase, Client, RequestFactory
from django.db.models import QuerySet
from django.template import Template

from . import views
from .models import Alimentador, Item
from django.contrib.auth.models import User


class TestAlimentador(TestCase):
    def setUp(self):
        self.yttitle = 'DjMaRiiO'
        self.id = 'UCi7TVXyvrIwqeS9tfYD8UDA'

    def test_crear_alimentador(self):
        alimentador = Alimentador(title=self.yttitle, id=self.id)
        alimentador.save()


class TestViews(TestCase):
    def setUp(self):
        self.yttitle = 'DjMaRiiO'
        self.id = 'UCi7TVXyvrIwqeS9tfYD8UDA'
        self.rdtitle = 'Ask Reddit...'
        self.idr = 'AskReddit'
        self.c=Client()
        self.request_factory = RequestFactory()
        self.user=User.objects.create_user(username = "test",email="test@test.com", password = "testtest")
    def test_miscosas(self):
        self.assertEqual(self.client.get('/miscosas/').status_code, 200)

    def test_info(self):
        self.assertEqual(self.client.get('/miscosas/info').status_code, 200)

    def test_alimyt(self):
        self.assertEqual(self.client.post('/miscosas/youtube', {'id': self.id, 'Alimentador': "Ver"}).status_code, 302)
        Alimentador.objects.get(title=self.yttitle)
        self.assertEqual(self.client.get('/miscosas/youtube/' + self.id).status_code, 200)

    def test_alimrd(self):
        self.assertEqual(self.client.post('/miscosas/reddit', {'id': self.idr, 'Alimentador': "Ver"}).status_code, 302)
        Alimentador.objects.get(title=self.rdtitle)

    def test_item(self):
        self.assertEqual(self.client.post('/miscosas/youtube', {'id': self.id, 'Alimentador': "Ver"}).status_code, 302)
        alimentador = Alimentador.objects.get(title=self.yttitle)
        self.item= Item.objects.filter(alimentador = alimentador)[0]
        self.assertEqual(self.client.get('/miscosas/youtube/' + alimentador.id + '/' + self.item.id).status_code, 200)

    def test_alimentadores(self):
        self.assertEqual(self.client.get('/miscosas/alimentadores').status_code, 200)

    def test_usuarios(self):
        self.assertEqual(self.client.get('/miscosas/usuarios').status_code, 200)

    def test_404(self):
        self.assertEqual(self.client.get('/cosassinexistencia').status_code,404)

    def test_register(self):
        self.assertEqual(self.client.get('/miscosas/register').status_code, 200)
