# Generated by Django 3.0.3 on 2020-05-20 14:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MisCosas', '0002_auto_20200516_1045'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aliment_select',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Alimentador',
            fields=[
                ('id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=64, null=True)),
                ('link', models.CharField(max_length=64, null=True)),
                ('num_items', models.IntegerField(default=0)),
                ('puntuacion', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('title', models.CharField(max_length=64, null=True)),
                ('link', models.CharField(max_length=64, null=True)),
                ('id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('description', models.CharField(max_length=256, null=True)),
                ('positivos', models.IntegerField(default=0)),
                ('negativos', models.IntegerField(default=0)),
                ('alimentador', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Alimentador')),
            ],
        ),
        migrations.DeleteModel(
            name='Page',
        ),
    ]
