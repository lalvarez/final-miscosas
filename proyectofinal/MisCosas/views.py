from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader, Context, RequestContext
from django.views.decorators.csrf import csrf_exempt
import urllib.request
from .ytchannel import YTChannel
from .redditnotice import Reddit
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from .models import Alimentador, Item, Vote, Comment, Image, Style
from .forms import UploadImageForm
from django.contrib.auth.models import User
from django.conf import settings
from django.template.loader import get_template
import json
from django.core import serializers
from django.views.defaults import page_not_found
@csrf_exempt

# ...

def index(request):
    # Creamos el formulario de autenticación vacío
    if request.method == 'GET':
        format = request.GET.get('format')
        if format:
            if format == 'xml':
                return HttpResponse(docxml(request.user), status=200, content_type="application/xml")
            else:
                return HttpResponse(docjson(request.user), status=200, content_type="application/json")

        Alimentadores = Alimentador.objects.all()
        top10 =Item.objects.order_by('-puntuacion')[:10]
        itemvoted = []
        if request.user.is_authenticated:
            imagen = Image.objects.get(user = request.user).photo
            votes = Vote.objects.filter(user = request.user)
            for vote in votes:
                itemvoted.append(vote.item)
            top5 = votes[:5]
        else:
            imagen = Image.objects.none()
            votes = Vote.objects.none()
            top5 = votes[:]
        if request.user.is_authenticated:
            style = Style.objects.get(user = request.user)
        else:
            style = Style.objects.none()

        # Si llegamos al final renderizamos el formulario
        return render(request, "pages/index.html", {'alimentadores': Alimentadores, 'imagen': imagen,
        'style': style, 'votes':votes, 'top10': top10 , 'itemvoted': itemvoted, 'top5': top5})

def info(request):
    user = request.user
    try:
        imagen = Image.objects.get(user=user).photo
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    return render(request, "pages/info.html", {'imagen': imagen,
    'style': style})
def docxml(user):
    template = get_template("index.xml")
    alimentadores = Alimentador.objects.filter(selected=True)
    top10 = Item.objects.order_by('-puntuacion')[:10]
    if user.is_authenticated:
        top5 = Vote.objects.filter(user=user)[:5]
        c = {"user":user,"alimentadores" : alimentadores, "top10" : top10, "top5":top5}
    else:
        c = {"alimentadores" : alimentadores, "top10" : top10}
    respuesta = template.render(c)
    return HttpResponse(respuesta, status=200, content_type="application/xml")

def docjson(user):
    all = [*Alimentador.objects.filter(selected=True), *Item.objects.order_by('-puntuacion')[:10]]
    indexjson = serializers.serialize('json', all)
    if user.is_authenticated:
        all = [*Alimentador.objects.filter(selected=True), *Item.objects.order_by('-puntuacion')[:10], *Vote.objects.filter(user=user)[:5]]
        indexjson = serializers.serialize('json', all)
    return indexjson


def alimentadores(request):
    Alimentadores = Alimentador.objects.all()
    user =  request.user
    try:
        imagen = Image.objects.get(user=user).photo
        print(imagen)
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    content = {'user': request.user, 'alimentadores': Alimentadores, 'imagen': imagen, 'style': style, }
    return render(request, 'pages/alimentadores.html', content)

def youtubeal(request):
    Alimentadores = Alimentador.objects.all()
    user = request.user
    if request.method == 'POST':
        id = request.POST['id']
        try:
            channel = Alimentador.objects.get(id=id)
            if id != '':
                xml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
                xmlStream = urllib.request.urlopen(xml)
                canal = YTChannel(xmlStream)
                channel = Alimentador.objects.get(id=id)

                items = Item.objects.filter(alimentador=channel)
                channel.num_items = len(items)
                channel.selected = True;
                channel.save()
        except:

            if id != '':
                xml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + id
                xmlStream = urllib.request.urlopen(xml)
                canal = YTChannel(xmlStream)
                channel = Alimentador.objects.get(id=id)

                items = Item.objects.filter(alimentador=channel)
                channel.num_items = len(items)
                channel.selected = True;
                channel.save()

    return redirect('/miscosas/youtube/' + id)

def youtubechannel(request, id):
        user = request.user
        try:
            imagen = Image.objects.get(user=user).photo
        except:
            imagen = Image.objects.none()
        if request.user.is_authenticated:
            style = Style.objects.get(user = request.user)
        else:
            style = Style.objects.none()

        channel = Alimentador.objects.get(id=id)

        content = {'channel': channel, 'videos': Item.objects.filter(alimentador=channel).order_by('title'),
                        'user': request.user, 'imagen': imagen, 'style': style}
        return render(request, 'pages/youtube.html', content)

def youtubevid(request, canal_id, item_id):
    Alimentadores = Alimentador.objects.all()
    user = request.user
    try:
        imagen = Image.objects.get(user=user).photo
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    video = get_object_or_404(Item, id=item_id)
    embeddedvid = "https://www.youtube.com/embed/" + item_id
    try:
        Voto = Vote.objects.filter(user= request.user).get(item = video)
    except:
        Voto = Vote.objects.none()
    Comments = Comment.objects.filter(item=video)


    content = {'video': video, 'embeddedvid': embeddedvid, 'alimentadores': Alimentadores,
                'voto': Voto, 'Comments': Comments, 'imagen': imagen, 'style': style}



    return render(request, 'pages/video.html', content)

def pag_404_not_found(request, exception):
   response = page_not_found(request, "pages/404.html")
   response.status_code=404
   return response

def reddital(request):
    if request.method == 'POST':
        id = request.POST['id']
        try:
            subreddit = Alimentador.objects.get(id=id)
            if id != '':
                xml = "http://www.reddit.com/r/" + id + ".rss"
                xmlStream = urllib.request.urlopen(xml)
                hilo = Reddit(xmlStream)
                subreddit = Alimentador.objects.get(id=id)

                items = Item.objects.filter(alimentador=subreddit)
                subreddit.num_items = len(items)
                subreddit.selected = True;
                subreddit.save()
        except:

            if id != '':
                xml = "http://www.reddit.com/r/" + id + ".rss"
                xmlStream = urllib.request.urlopen(xml)
                hilo = Reddit(xmlStream)
                subreddit = Alimentador.objects.get(id=id)

                items = Item.objects.filter(alimentador=subreddit)
                subreddit.num_items = len(items)
                subreddit.selected = True;
                subreddit.save()




    return redirect('/miscosas/reddit/' + id)



def subreddit(request, id):
    Alimentadores = Alimentador.objects.all()
    user = request.user
    try:
        imagen = Image.objects.get(user=user).photo
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    subreddit = Alimentador.objects.get(id=id)

    content = {'subreddit': subreddit, 'notices': Item.objects.filter(alimentador=subreddit).order_by('title'),
                        'user': request.user, 'alimentadores': Alimentadores, 'imagen': imagen, 'style':style}

    return render(request, 'pages/reddit.html', content)


def redditnot(request, subreddit_id, item_id):
    Alimentadores = Alimentador.objects.all()
    user = request.user
    try:
        imagen = Image.objects.get(user=user).photo
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    notice = get_object_or_404(Item, id=item_id)
    try:
        Voto = Vote.objects.filter(user= request.user).get(item = notice)
    except:
        Voto = Vote.objects.none()
    Comments = Comment.objects.filter(item=notice)


    content = {'notice': notice, 'alimentadores': Alimentadores,
                'voto': Voto, 'Comments': Comments, 'imagen': imagen, 'style': style}
    return render(request, 'pages/notice.html', content)

def delete(request):
    if request.method == 'POST':
        id = request.POST['id']
        channel = Alimentador.objects.get(id=id)
        if(channel.selected == True):
            channel.selected = False
        else:
            channel.selected = True

        channel.save()
        return redirect('/miscosas')




    return render(request, 'pages/youtube.html', content)

def usuarios(request):
    imagenes = Image.objects.all()
    alimentadores = Alimentador.objects.all()
    user = request.user
    try:
        imagen = Image.objects.get(user=user).photo
    except:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    content = {'alimentadores': alimentadores,'imagenes': imagenes, 'imagen': imagen, 'style': style}



    return render(request, 'pages/usuarios.html', content)


def vote(request, item_id):
    if request.method == 'POST':
        item = Item.objects.get(id=item_id)
        vote = request.POST['vote']
        user = request.user

        try:
            votoant = Vote.objects.filter(user=user).get(item = item)
            if votoant.voto == 1 and vote == 'dislike':
                item.positivos = item.positivos -1
                item.negativos = item.negativos + 1
                item.puntuacion =  item.positivos - item.negativos
                item.save()
                votoant.voto = -1
                votoant.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion - 2
                item.alimentador.save()
            if votoant.voto == -1 and vote == 'like':
                item.negativos = item.negativos -1
                item.positivos = item.positivos + 1
                item.puntuacion =  item.positivos - item.negativos
                item.save()
                votoant.voto = 1
                votoant.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion + 2
                item.alimentador.save()

        except:

            if vote == 'like':
                item.positivos = item.positivos + 1
                item.puntuacion =  item.positivos - item.negativos
                item.save()
                voted = Vote(user = user, item = item, voto = 1)
                voted.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion + 1
                item.alimentador.save()
            else:
                item.negativos = item.negativos + 1
                item.puntuacion =  item.positivos - item.negativos
                item.save()
                voted = Vote(user = user, item = item, voto = -1)
                voted.save()
                item.alimentador.puntuacion = item.alimentador.puntuacion - 1
                item.alimentador.save()
        if item.alimentador.tipo == 'yt':
            url = '/miscosas/youtube/' + item.alimentador.id + '/' + item_id
        elif item.alimentador.tipo == 'rd':
            url = '/miscosas/reddit/' + item.alimentador.id + '/' + item_id
        return redirect(url)

def comment(request, item_id):
    if request.method == 'POST':
        item = Item.objects.get(id=item_id)
        title = request.POST['Title']
        comment = request.POST['Comment']
        photo = request.POST['Photo']

        user = request.user

        commentfin = Comment(user=user, item = item, title = title, comment = comment, photourl= photo)
        commentfin.save()

        if item.alimentador.tipo == 'yt':
            url = '/miscosas/youtube/' + item.alimentador.id + '/' + item_id
        elif item.alimentador.tipo == 'rd':
            url = '/miscosas/reddit/' + item.alimentador.id + '/' + item_id
        return redirect(url)
def user(request, username):

    user = User.objects.get(username = username)
    image = Image.objects.get(user = user)
    vote = Vote.objects.filter(user = user)
    comments = Vote.objects.filter(user= user)
    if request.user.is_authenticated:
        imagen = Image.objects.get(user = request.user).photo
    else:
        imagen = Image.objects.none()
    if request.user.is_authenticated:
        style = Style.objects.get(user = request.user)
    else:
        style = Style.objects.none()
    form = UploadImageForm()
    if request.method == 'POST':
        form = UploadImageForm(request.POST, request.FILES)
        if form.is_valid():

            image.photo = request.FILES.get('photo')
            image.save()


    content = {'form': form, 'image': image, 'votes': vote, 'comments': comments, 'imagen': imagen, 'style':style}

    return render(request, "pages/user.html", content)


def register(request):
    # Creamos el formulario de autenticación vacío
    form = UserCreationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = UserCreationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():

            # Creamos la nueva cuenta de usuario
            user = form.save()
            estilo =  Style(user = user)
            estilo.save()
            imagen = Image(user= user)
            imagen.save()
            # Si el usuario se crea correctamente
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/miscosas')

    # Si llegamos al final renderizamos el formulario
    return render(request, "pages/register.html", {'form': form})

def logout_view(request):
    logout(request)
    return redirect("/miscosas")

def loggedIn(request):
    if request.user.is_authenticated:
        return redirect ('/miscosas')
    else:
        username1 = request.POST['username']
        password = request.POST['password']

        # Verificamos las credenciales del usuario
        user = authenticate(username=username1, password=password)

        # Si existe un usuario con ese nombre y contraseña
        if user is not None:
            # Hacemos el login manualmente
            do_login(request, user)
            try:
                imagen = Image.objects.get(user=user)
            except:
                imagen = Image(user= user)
                imagen.save()
            try:
                estilo = Style.objects.get(user=user)
            except:
                estilo = Style(user= user)
                estilo.save()
            # Y le redireccionamos a la portada
            return redirect('/miscosas')
        else:
            return redirect('/miscosas')
def oscuro(request):
    if request.method == 'POST':
        style = Style.objects.get(user = request.user)
        if request.POST['mode'] == 'DarkMode':
            style.oscuro = True;
        else:
            style.oscuro = False;

        style.save()
    return redirect("/miscosas/" + request.user.username)


def letras(request):
    if request.method == 'POST':
        style = Style.objects.get(user = request.user)
        if request.POST['Fuente'] != style.letra:
            style.letra = request.POST['Fuente'];
        style.save()
    return redirect("/miscosas/" + request.user.username)
